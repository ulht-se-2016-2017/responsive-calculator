#Responsive Calculator

Uma calculadora HTML, desenhada de forma responsive para ocupar sempre todo o écran.

Experimentar acedendo a:
http://palves-ulht.bitbucket.io/responsive-calculator

Esta calculadora faz parte dos conteúdos da disciplina de Sistemas Embebidos da Universidade Lusófona.

## Instruções
Esta calculadora não está completa.

Deverá fazer fork deste repositório para um respositório com o nome:
*username*.bitbucket.org

Nesse repositório, deverá fazer as seguintes alterações na calculadora:

* Implementar as funções em falta
* Criar um botão "C" (Clear) que limpa o resultado (coloca-o a zero).
* Alterar o layout do keypad para ter as operações na horizontal, na última linha
* Criar um botão "Del" que apaga o último dígito inserido

Poderá ver os resultados das suas alterações acedendo a:
http://*username*.bitbucket.io/responsive-calculator