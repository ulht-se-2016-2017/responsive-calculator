var result = document.querySelector(".result");
var btns = document.querySelector(".keypad");

btns.addEventListener("click",function(e) {
    var btn = e.target;

    if (btn.className.indexOf("number") > -1) {   // the button is a number
        if (result.innerHTML == "0") {
            result.innerHTML = btn.innerHTML;
        } else {
            result.innerHTML += btn.innerHTML;
        }
    } else {   // the button is an operation
        var operation = btn.innerHTML;
        switch (operation) {
            case "sqr":
                result.innerHTML = Math.sqrt(result.innerHTML);
                break;
            case "pow":
                result.innerHTML = Math.pow(result.innerHTML, 2);
                break;
        }
    }
});